"use strict";
var router_1 = require('@angular/router');
var emergency_numbers_list_component_1 = require("./emergency-number/emergency-numbers-list.component");
var how_works_component_1 = require("./about/how-works.component");
var sos_category_list_component_1 = require("./sos-category/sos-category-list.component");
var sos_particular_category_list_component_1 = require("./sos-particular/sos-particular-category-list.component");
var category_form_component_1 = require("./admin/category-form.component");
var sos_particular_form_component_1 = require("./admin/sos-particular-form.component");
var sos_particular_item_list_component_1 = require("./sos-particular/sos-particular-item-list.component");
var admin_home_component_1 = require("./admin/admin-home.component");
var sos_request_component_1 = require("./sos-request.component");
exports.routes = [
    {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
    },
    { path: 'home', component: sos_category_list_component_1.SOSCategoryListComponent },
    { path: 'numeros-emergencia', component: emergency_numbers_list_component_1.EmergencyNumbersComponent },
    { path: 'how-works', component: how_works_component_1.HowWorksComponent },
    { path: 'lista-categorias-servicos-particulares', component: sos_particular_category_list_component_1.SOSParticularCategoryListComponent },
    { path: 'cadastrar-categorias', component: category_form_component_1.CategoryFormComponent },
    { path: 'cadastrar-servicos-particulares', component: sos_particular_form_component_1.SOSParticularFormComponent },
    { path: 'lista-itens-servicos-particulares/:category/:icon/:back', component: sos_particular_item_list_component_1.SOSParticularItemListComponent },
    { path: 'admin', component: admin_home_component_1.AdminHomeComponent },
    { path: 'chamado/:title/:icon/:background-color/:text-color', component: sos_request_component_1.SOSRequestComponent }
];
exports.APP_ROUTER_PROVIDERS = [
    router_1.provideRouter(exports.routes)
];
//# sourceMappingURL=app.routes.js.map