"use strict";
var RoutesClientUtil = (function () {
    function RoutesClientUtil() {
    }
    RoutesClientUtil.CATEGORIES_REGISTER = "/cadastrar-categorias";
    RoutesClientUtil.PARTICULAR_SERVICES_REGISTER = "/cadastrar-servicos-particulares";
    return RoutesClientUtil;
}());
exports.RoutesClientUtil = RoutesClientUtil;
//# sourceMappingURL=routes-client.util.js.map