import {Component} from "@angular/core";
import {SOSCategoryItemComponent} from "../sos-category/sos-category-item.component";

@Component({
    selector: 'emergency-numbers',
    templateUrl: '/app/emergency-number/emergency-numbers-list.html',
    directives: [SOSCategoryItemComponent]
})
export class EmergencyNumbersComponent{
    
}
