import { Injectable } from '@angular/core';
import {RoutesServerUtil} from "../util/routes-server.util";
import {Observable} from "rxjs/Rx";
import {ParticularService} from "../model/particular-service.model";
import {Http, Response} from "@angular/http";

@Injectable()
export class ParticularServiceListService {

    private url = RoutesServerUtil.URL_API + "particular-services";

    constructor(private http: Http) { }

    listParticularByCategory(value:string): Observable<ParticularService[]>{
        return this.http.get(this.url + "/property/category.name/value/" + value)
            .map(this.handleSuccess)
            .catch(this.handleError);
    }

    private handleSuccess(response: Response){
        let body = response.json();
        console.log("Response Type: " + response.type);
        console.log("Response: " + response);
        console.log("Sucesso: " + body);
        console.log("Data: " + JSON.stringify(body));
        return body || { };
    }

    private handleError (error: any) {
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }


}
