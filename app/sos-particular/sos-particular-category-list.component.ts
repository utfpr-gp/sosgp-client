import {Component} from "@angular/core";
import {SOSCategoryItemComponent} from "../sos-category/sos-category-item.component";
import {ROUTER_DIRECTIVES} from "@angular/router";
@Component({
    selector: 'particular-services',
    templateUrl: '/app/sos-particular/sos-particular-category-list.html',
    directives:[SOSCategoryItemComponent, ROUTER_DIRECTIVES]
})
export class SOSParticularCategoryListComponent{
    
}
