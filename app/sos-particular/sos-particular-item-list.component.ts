import {Component, OnInit, OnDestroy} from '@angular/core';
import {ROUTER_DIRECTIVES, ActivatedRoute} from "@angular/router";
import {SOSParticularItemComponent} from "./sos-particular-item.component";
import {ParticularServiceListService} from "./sos-particular-item-list.service";
import {ParticularService} from "../model/particular-service.model";
import {Router} from "@angular/router";
import {TitleComponent} from "../shared/components/title.component";

@Component({
    selector: 'detail-service',
    templateUrl: '/app/sos-particular/sos-particular-item-list.html',
    directives:[ROUTER_DIRECTIVES, SOSParticularItemComponent, TitleComponent],
    precompile: [TitleComponent],
    providers:[ParticularServiceListService]
})
export class SOSParticularItemListComponent implements OnInit, OnDestroy {

    private sub: any;
    category:string;
    icon:string;
    back:string;
    entities:ParticularService[] = [];
    errorMessage:string = "Houve um erro!";

    constructor(private service: ParticularServiceListService,
                private route: ActivatedRoute,
                private router: Router
    ) { }

    listEntities(){
        this.service.listParticularByCategory(this.category).subscribe(
            entities => {
                this.entities = entities;
            },
            error => this.errorMessage = <any> error
        );
    }

    ngOnInit() {
        this.sub = this.route.params.subscribe(
            params => {
                this.category = decodeURI(params['category']);
                this.icon = params['icon'];
                this.back = decodeURI(params['back']);
                this.listEntities();
            }
        );
    }

    ngOnDestroy() {
        this.sub.unsubscribe();
    }
}