"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require("@angular/router");
var sos_particular_item_component_1 = require("./sos-particular-item.component");
var sos_particular_item_list_service_1 = require("./sos-particular-item-list.service");
var router_2 = require("@angular/router");
var title_component_1 = require("../shared/components/title.component");
var SOSParticularItemListComponent = (function () {
    function SOSParticularItemListComponent(service, route, router) {
        this.service = service;
        this.route = route;
        this.router = router;
        this.entities = [];
        this.errorMessage = "Houve um erro!";
    }
    SOSParticularItemListComponent.prototype.listEntities = function () {
        var _this = this;
        this.service.listParticularByCategory(this.category).subscribe(function (entities) {
            _this.entities = entities;
        }, function (error) { return _this.errorMessage = error; });
    };
    SOSParticularItemListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.sub = this.route.params.subscribe(function (params) {
            _this.category = decodeURI(params['category']);
            _this.icon = params['icon'];
            _this.back = decodeURI(params['back']);
            _this.listEntities();
        });
    };
    SOSParticularItemListComponent.prototype.ngOnDestroy = function () {
        this.sub.unsubscribe();
    };
    SOSParticularItemListComponent = __decorate([
        core_1.Component({
            selector: 'detail-service',
            templateUrl: '/app/sos-particular/sos-particular-item-list.html',
            directives: [router_1.ROUTER_DIRECTIVES, sos_particular_item_component_1.SOSParticularItemComponent, title_component_1.TitleComponent],
            precompile: [title_component_1.TitleComponent],
            providers: [sos_particular_item_list_service_1.ParticularServiceListService]
        }), 
        __metadata('design:paramtypes', [sos_particular_item_list_service_1.ParticularServiceListService, router_1.ActivatedRoute, router_2.Router])
    ], SOSParticularItemListComponent);
    return SOSParticularItemListComponent;
}());
exports.SOSParticularItemListComponent = SOSParticularItemListComponent;
//# sourceMappingURL=sos-particular-item-list.component.js.map