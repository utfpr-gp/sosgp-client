"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var neighborhood_model_1 = require("../model/neighborhood.model");
var SOSParticularItemComponent = (function () {
    function SOSParticularItemComponent() {
    }
    SOSParticularItemComponent.prototype.ngOnInit = function () { };
    __decorate([
        core_1.Input("name"), 
        __metadata('design:type', String)
    ], SOSParticularItemComponent.prototype, "name", void 0);
    __decorate([
        core_1.Input("neigh"), 
        __metadata('design:type', neighborhood_model_1.Neighborhood)
    ], SOSParticularItemComponent.prototype, "neighborhood", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], SOSParticularItemComponent.prototype, "description", void 0);
    __decorate([
        core_1.Input(), 
        __metadata('design:type', String)
    ], SOSParticularItemComponent.prototype, "phone", void 0);
    SOSParticularItemComponent = __decorate([
        core_1.Component({
            selector: 'detail-item',
            templateUrl: '/app/sos-particular/sos-particular-item.html'
        }), 
        __metadata('design:paramtypes', [])
    ], SOSParticularItemComponent);
    return SOSParticularItemComponent;
}());
exports.SOSParticularItemComponent = SOSParticularItemComponent;
//# sourceMappingURL=sos-particular-item.component.js.map