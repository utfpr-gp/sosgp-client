import {Component, OnInit, Input} from '@angular/core';
import {Neighborhood} from "../model/neighborhood.model";

@Component({
    selector: 'detail-item',
    templateUrl: '/app/sos-particular/sos-particular-item.html'
})
export class SOSParticularItemComponent implements OnInit {

    constructor() { }

    ngOnInit() { }

    @Input("name") name:string;
    @Input("neigh") neighborhood:Neighborhood;
    @Input() description:string;
    @Input() phone:string;

}
