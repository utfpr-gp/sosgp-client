"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var location_model_1 = require("../model/location.model");
var city_service_1 = require("../shared/services/city.service");
var MapComponent = (function () {
    function MapComponent(cityService) {
        this.cityService = cityService;
        this.addressChangeEmitter = new core_1.EventEmitter();
        this.location = new location_model_1.Location();
        this.isSupported = true;
        this.cities = [];
    }
    MapComponent.prototype.ngOnInit = function () {
        this.listCities();
    };
    MapComponent.prototype.ngAfterViewInit = function () {
        this.initMap();
    };
    MapComponent.prototype.initMap = function () {
        var _this = this;
        if (navigator.geolocation) {
            //mostra GP por default, uma vez que o usuário pode não dar permissão para descobrir suas coordenadas
            this.showGuarapuava();
            navigator.geolocation.getCurrentPosition(function (position) {
                _this.showPosition(position);
            }, function (error) {
                _this.showError(error);
            });
        }
        else {
            this.isSupported = false;
            this.errorText = "Não suportado!";
        }
    };
    MapComponent.prototype.showPosition = function (position) {
        var _this = this;
        var lat = position.coords.latitude;
        var lon = position.coords.longitude;
        this.location.lat = lat;
        this.location.lon = lon;
        var latlon = { lat: lat, lng: lon };
        var mapProp = {
            center: new google.maps.LatLng(lat, lon),
            zoom: 18,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        this.map = new google.maps.Map(document.getElementById('mapHolder'), mapProp);
        this.marker = new google.maps.Marker({
            position: latlon,
            draggable: true,
            title: "Estou aqui!"
        });
        this.marker.setMap(this.map);
        var infoWindow = new google.maps.InfoWindow({ map: this.map });
        infoWindow.setPosition(latlon);
        infoWindow.setContent('Estou aqui!');
        infoWindow.open(this.map, this.marker);
        //consegue o endereço a partir das coordenadas
        this.geocoder = new google.maps.Geocoder();
        //evento de drag do marker
        google.maps.event.addListener(this.marker, 'dragend', function () {
            _this.geocodeLatLng(_this.geocoder, _this.marker.getPosition());
        });
        //chamada é assincrona
        var latlng = { lat: lat, lng: lon };
        this.geocodeLatLng(this.geocoder, latlng);
    };
    MapComponent.prototype.showGuarapuava = function () {
        var lat = -25.393;
        var lon = -51.465;
        var mapProp = {
            center: new google.maps.LatLng(lat, lon),
            zoom: 17,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById('mapHolder'), mapProp);
        this.marker = new google.maps.Marker({
            position: new google.maps.LatLng(lat, lon),
            draggable: true,
            map: map,
            title: "Estou aqui!"
        });
        //chamada é assincrona
        var latlng = { lat: lat, lng: lon };
        this.geocoder = new google.maps.Geocoder();
        this.geocodeLatLng(this.geocoder, latlng);
    };
    MapComponent.prototype.showError = function (error) {
        //caso haja erro, mostrar Guarapuava como default
        this.showGuarapuava();
        /*
        this.isSupported = false;
        switch(error.code) {
            case error.PERMISSION_DENIED:
                this.errorText = "Você precisa permitir o acesso a sua localização."
                break;
            case error.POSITION_UNAVAILABLE:
                this.errorText = "Location information is unavailable."
                break;
            case error.TIMEOUT:
                this.errorText = "The request to get user location timed out."
                break;
            case error.UNKNOWN_ERROR:
                this.errorText = "An unknown error occurred."
                break;
        }
        */
    };
    MapComponent.prototype.onChangeCity = function (value) {
        this.location.city = value;
    };
    MapComponent.prototype.onSearchAddress = function () {
        var completeAddress = this.location.street + ", " + this.location.number + " , " + this.location.city;
        this.geocodeAddress(completeAddress);
    };
    MapComponent.prototype.onSubmit = function () {
    };
    MapComponent.prototype.listCities = function () {
        var _this = this;
        this.cityService.listCities().subscribe(function (entities) {
            _this.cities = entities;
        }, function (error) { return _this.errorMessage = error; });
    };
    MapComponent.prototype.notifyLocation = function (street, number, neigh, city, state) {
        //emite a posição e endereço para o parente
        /*
         this.addressChange.emit({
         lat : this.lat,
         lon : this.lon,
         address : "GP"
         });
         */
        this.location = new location_model_1.Location(street, number, neigh, city, state);
        this.location.city = city;
        this.addressChangeEmitter.emit(this.location);
    };
    /**
     *
     * Transforma um endereço em latlon
     * Marca no mapa o novo posicionamento
     *
     * @param geocoder
     * @param map
     * @param address
     */
    MapComponent.prototype.geocodeAddress = function (address) {
        var _this = this;
        this.geocoder.geocode({ 'address': address }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                _this.map.setCenter(results[0].geometry.location);
                //var latlng = new google.maps.LatLng(results[0].geometry.location.lat, results[0].geometry.location.lng);
                _this.marker.setMap(null);
                _this.marker = null;
                _this.marker = new google.maps.Marker({
                    position: results[0].geometry.location,
                    draggable: true,
                    title: "Estou aqui!"
                });
                _this.marker.setMap(_this.map);
                var infoWindow = new google.maps.InfoWindow({ map: _this.map });
                infoWindow.setPosition(results[0].geometry.location);
                infoWindow.setContent('Estou aqui!');
                infoWindow.open(_this.map, _this.marker);
            }
            else {
                alert("Geocode was not successful for the following reason: " + status);
            }
        });
    };
    /**
     *
     * Chamada é assincrona.
     * Quando o resultado é obtido, uma função é chamada para notificar o parent.
     * Retorna o endereço completo a partir da latitude e longitude.
     *
     * @param geocoder
     * @param lat
     * @param lon
     */
    MapComponent.prototype.geocodeLatLng = function (geocoder, latlng) {
        var _this = this;
        geocoder.geocode({ 'location': latlng }, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    var street = void 0;
                    var number = void 0;
                    var neigh = void 0;
                    var city = void 0;
                    var state = void 0;
                    var array = results[0].address_components;
                    for (var i = 0; i < array.length; i++) {
                        var type = array[i].types[0];
                        if (type === "street_number") {
                            number = array[i].short_name;
                        }
                        else if (type === "route") {
                            street = array[i].short_name;
                        }
                        else if (type === "political") {
                            neigh = array[i].short_name;
                        }
                        else if (type === "locality") {
                            city = array[i].short_name;
                        }
                        else if (type === "administrative_area_level_1") {
                            state = array[i].short_name;
                        }
                    }
                    _this.notifyLocation(street, number, neigh, city, state);
                }
                else {
                    alert('No results found');
                }
            }
            else {
                alert('Geocoder failed due to: ' + status);
            }
            return {};
        });
    };
    __decorate([
        core_1.Output(), 
        __metadata('design:type', Object)
    ], MapComponent.prototype, "addressChangeEmitter", void 0);
    MapComponent = __decorate([
        core_1.Component({
            selector: 'sos-map',
            templateUrl: '/app/map/map.html'
        }), 
        __metadata('design:paramtypes', [city_service_1.CityService])
    ], MapComponent);
    return MapComponent;
}());
exports.MapComponent = MapComponent;
//# sourceMappingURL=map.component.js.map