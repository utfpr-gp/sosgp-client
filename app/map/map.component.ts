import {Component, OnInit, AfterViewInit, Input, Output, EventEmitter} from '@angular/core';
import {Location} from "../model/location.model";
import {CityService} from "../shared/services/city.service";
import {City} from "../model/city.model";

declare var google:any;

@Component({
    selector: 'sos-map',
    templateUrl: '/app/map/map.html'
})
export class MapComponent implements OnInit, AfterViewInit {

    @Output() addressChangeEmitter = new EventEmitter();

    location:Location = new Location();
    isSupported:boolean = true;
    errorText:string;
    cities:City[] = [];
    errorMessage:string;

    map:any;
    geocoder:any;
    marker:any;

    constructor(private cityService: CityService) { }

    ngOnInit() {
        this.listCities();
    }

    ngAfterViewInit(){
        this.initMap();
    }

    initMap() {
        if (navigator.geolocation) {
            //mostra GP por default, uma vez que o usuário pode não dar permissão para descobrir suas coordenadas
            this.showGuarapuava();

            navigator.geolocation.getCurrentPosition(
                (position) => {
                    this.showPosition(position);
                },
                (error) => {
                    this.showError(error);
                }
            );
        }
        else {
            this.isSupported = false;
            this.errorText = "Não suportado!"
        }
    }

    showPosition(position){

        let lat = position.coords.latitude;
        let lon = position.coords.longitude;
        this.location.lat = lat;
        this.location.lon = lon;
        let latlon = {lat: lat, lng: lon};
        let mapProp = {
            center: new google.maps.LatLng(lat,lon),
            zoom:18,
            mapTypeId:google.maps.MapTypeId.ROADMAP
            //mapTypeId:google.maps.MapTypeId.SATELLITE
        };
        this.map = new google.maps.Map(document.getElementById('mapHolder'), mapProp);

        this.marker = new google.maps.Marker({
            position: latlon,
            draggable:true,
            title:"Estou aqui!"
        });
        this.marker.setMap(this.map);

        var infoWindow = new google.maps.InfoWindow({map: this.map});
        infoWindow.setPosition(latlon);
        infoWindow.setContent('Estou aqui!');
        infoWindow.open(this.map, this.marker);

        //consegue o endereço a partir das coordenadas
        this.geocoder = new google.maps.Geocoder();

        //evento de drag do marker
        google.maps.event.addListener(this.marker, 'dragend', () => {
            this.geocodeLatLng(this.geocoder, this.marker.getPosition());
        });

        //chamada é assincrona
        var latlng = {lat: lat, lng: lon};
        this.geocodeLatLng(this.geocoder, latlng);
    }

    showGuarapuava(){
        let lat = -25.393;
        let lon = -51.465;
        let mapProp = {
            center: new google.maps.LatLng(lat,lon),
            zoom:17,
            mapTypeId:google.maps.MapTypeId.ROADMAP
        };
        let map = new google.maps.Map(document.getElementById('mapHolder'), mapProp);
        this.marker = new google.maps.Marker({
            position: new google.maps.LatLng(lat,lon),
            draggable:true,
            map: map,
            title:"Estou aqui!"
        });

        //chamada é assincrona
        var latlng = {lat: lat, lng: lon};
        this.geocoder = new google.maps.Geocoder();
        this.geocodeLatLng(this.geocoder, latlng);
    }

    showError(error){
        //caso haja erro, mostrar Guarapuava como default
        this.showGuarapuava();
        /*
        this.isSupported = false;
        switch(error.code) {
            case error.PERMISSION_DENIED:
                this.errorText = "Você precisa permitir o acesso a sua localização."
                break;
            case error.POSITION_UNAVAILABLE:
                this.errorText = "Location information is unavailable."
                break;
            case error.TIMEOUT:
                this.errorText = "The request to get user location timed out."
                break;
            case error.UNKNOWN_ERROR:
                this.errorText = "An unknown error occurred."
                break;
        }
        */
    }

    onChangeCity(value){
        this.location.city = value;
    }

    onSearchAddress(){
        let completeAddress = this.location.street + ", " + this.location.number + " , " + this.location.city;
        this.geocodeAddress(completeAddress);
    }

    onSubmit(){

    }

    listCities(){
        this.cityService.listCities().subscribe(
            entities => {
                this.cities = entities;
            },
            error => this.errorMessage = <any> error
        )
    }

    notifyLocation(street:string, number:number, neigh:string, city:string, state:string){
        //emite a posição e endereço para o parente
        /*
         this.addressChange.emit({
         lat : this.lat,
         lon : this.lon,
         address : "GP"
         });
         */

        this.location = new Location(street, number, neigh, city, state);
        this.location.city = city;
        this.addressChangeEmitter.emit(this.location);
    }

    /**
     *
     * Transforma um endereço em latlon
     * Marca no mapa o novo posicionamento
     *
     * @param geocoder
     * @param map
     * @param address
     */
    geocodeAddress(address){
        this.geocoder.geocode( { 'address': address}, (results, status) => {
            if (status == google.maps.GeocoderStatus.OK) {

                this.map.setCenter(results[0].geometry.location);

                //var latlng = new google.maps.LatLng(results[0].geometry.location.lat, results[0].geometry.location.lng);
                this.marker.setMap(null);
                this.marker = null;
                this.marker = new google.maps.Marker({
                    position: results[0].geometry.location,
                    draggable:true,
                    title:"Estou aqui!"
                });
                this.marker.setMap(this.map);

                var infoWindow = new google.maps.InfoWindow({map: this.map});
                infoWindow.setPosition(results[0].geometry.location);
                infoWindow.setContent('Estou aqui!');
                infoWindow.open(this.map, this.marker);

            }
            else {
                alert("Geocode was not successful for the following reason: " + status);
            }
        });
    }



    /**
     *
     * Chamada é assincrona.
     * Quando o resultado é obtido, uma função é chamada para notificar o parent.
     * Retorna o endereço completo a partir da latitude e longitude.
     *
     * @param geocoder
     * @param lat
     * @param lon
     */
    geocodeLatLng(geocoder, latlng):any {

        geocoder.geocode({'location': latlng}, (results, status) => {
            if (status === google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    let street:string;
                    let number:number;
                    let neigh: string;
                    let city: string;
                    let state: string;

                    let array:any[] = results[0].address_components;

                    for(let i = 0; i < array.length; i++){
                        let type = array[i].types[0];
                        if(type === "street_number"){
                            number = array[i].short_name;
                        }
                        else if(type === "route"){
                            street = array[i].short_name;
                        }
                        else if(type === "political"){
                            neigh = array[i].short_name;
                        }
                        else if(type === "locality"){
                            city = array[i].short_name;
                        }
                        else if(type === "administrative_area_level_1"){
                            state = array[i].short_name;
                        }
                    }

                    this.notifyLocation(street, number, neigh, city, state);


                }
                else {
                    alert('No results found');
                }
            }
            else {
                alert('Geocoder failed due to: ' + status);
            }
            return {};
        });

    }
}
