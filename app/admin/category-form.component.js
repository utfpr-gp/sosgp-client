"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var category_model_1 = require("../model/category.model");
var category_service_1 = require("./category.service");
var CategoryFormComponent = (function () {
    function CategoryFormComponent(categoryService) {
        this.categoryService = categoryService;
        this.errorMessage = "Houve um erro!";
        this.model = new category_model_1.Category();
        this.isUpdate = false;
        /**
         *
         * Usado para habilitar o botão de submit
         *
         * @type {boolean}
         */
        this.submitted = false;
        /**
         *
         * Usado para resetar o formulário.
         * O valor é mudado para falso e depois novamente para verdadeiro.
         * Com isso, o formulário é recriado e os campos resetados após a criação de um modelo.
         *
         * @type {boolean}
         */
        this.active = true;
        this.categories = [];
    }
    CategoryFormComponent.prototype.onSubmit = function () {
        this.submitted = true;
        if (this.isUpdate) {
            this.updateCategory(this.model);
        }
        else {
            this.saveCategory(this.model);
        }
    };
    Object.defineProperty(CategoryFormComponent.prototype, "diagnostic", {
        get: function () {
            return JSON.stringify(this.model);
        },
        enumerable: true,
        configurable: true
    });
    CategoryFormComponent.prototype.saveCategory = function (category) {
        var _this = this;
        if (!category) {
            return;
        }
        else {
            this.categoryService.addCategory(category).subscribe(function (cat) {
                _this.categories.push(cat);
                _this.resetForm();
            }, function (error) { return _this.errorMessage = error; });
        }
    };
    CategoryFormComponent.prototype.updateCategory = function (category) {
        var _this = this;
        if (!category) {
            return;
        }
        else {
            this.categoryService.addCategory(category).subscribe(function (cat) {
                console.log("Corrente Indice: " + _this.currentIndex);
                console.log("Size antes " + _this.categories.length);
                //remove o antigo
                _this.categories.splice(_this.currentIndex, 1);
                console.log("Size depois " + _this.categories.length);
                //adiciona o novo
                _this.categories.push(cat);
                _this.resetForm();
            }, function (error) { return _this.errorMessage = error; });
        }
    };
    CategoryFormComponent.prototype.deleteCategory = function (category, index) {
        var _this = this;
        this.categoryService.deleteCategory(category.id).subscribe(function (res) {
            _this.categories.splice(index, 1);
        }, function (error) { return _this.errorMessage = error; });
    };
    CategoryFormComponent.prototype.resetForm = function () {
        var _this = this;
        this.active = false;
        this.model.id = undefined;
        this.model.name = "";
        setTimeout(function () { return _this.active = true; }, 0);
        this.isUpdate = false;
    };
    CategoryFormComponent.prototype.handleSuccess = function (cat) {
        this.categories.push(cat);
        this.model.name = "";
        console.log("Categorias: " + this.categories);
    };
    CategoryFormComponent.prototype.getCategories = function () {
        var _this = this;
        this.categoryService.getCategories().subscribe(function (categories) { return _this.categories = categories; }, function (error) { return _this.errorMessage = error; });
    };
    CategoryFormComponent.prototype.newCategory = function () {
        this.model = new category_model_1.Category();
        this.isUpdate = false;
    };
    CategoryFormComponent.prototype.remove = function (category) {
        var index = this.categories.indexOf(category);
        console.log("Removido indice " + index);
        this.isUpdate = false;
        this.deleteCategory(category, index);
    };
    CategoryFormComponent.prototype.edit = function (category) {
        this.model.id = category.id;
        this.model.name = category.name;
        var index = this.categories.indexOf(category);
        this.currentIndex = index;
        this.isUpdate = true;
    };
    CategoryFormComponent.prototype.ngOnInit = function () {
        this.getCategories();
    };
    CategoryFormComponent = __decorate([
        core_1.Component({
            selector: "register-category",
            templateUrl: "/app/admin/category-form.html",
            providers: [category_service_1.CategoryService]
        }), 
        __metadata('design:paramtypes', [category_service_1.CategoryService])
    ], CategoryFormComponent);
    return CategoryFormComponent;
}());
exports.CategoryFormComponent = CategoryFormComponent;
//# sourceMappingURL=category-form.component.js.map