import { Component, OnInit } from '@angular/core';
import {ROUTER_DIRECTIVES} from "@angular/router";
import {SOSCategoryItemComponent} from "../sos-category/sos-category-item.component";

@Component({
    selector: 'admin',
    templateUrl: '/app/admin/admin-home.html',
    directives: [ROUTER_DIRECTIVES, SOSCategoryItemComponent]
})
export class AdminHomeComponent implements OnInit {
    constructor() { }

    ngOnInit() { }
}
