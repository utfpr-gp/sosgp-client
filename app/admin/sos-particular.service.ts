import {Injectable} from "@angular/core";
import {Http, Response, Headers, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Rx";
import {ParticularService} from "../model/particular-service.model";
import {RoutesServerUtil} from "../util/routes-server.util";
import {City} from "../model/city.model";
import {Category} from "../model/category.model";

@Injectable()
export class SOSParticularService{

    private url = RoutesServerUtil.URL_API + "particular-services";
    private urlCategories = RoutesServerUtil.URL_API + "categorias";

    constructor(private http: Http){
    }

    save(entity: ParticularService): Observable<ParticularService>{
        let body = JSON.stringify(entity);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.url, body, options)
            .map(this.handleSuccess)
            .catch(this.handleError);
    }

    update(entity: ParticularService): Observable<ParticularService>{
        let body = JSON.stringify(entity);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.put(this.url, body, options)
            .map(this.handleSuccess)
            .catch(this.handleError);
    }

    delete(id:number):Observable<{}>{
        let url:string = this.url + "/" + id;
        return this.http.delete(url)
            .map(response => {
                console.log("Response Type: " + response.type);
                console.log("Response: " + response);
                return {};
            })
            .catch(this.handleError);
    }

    listEntities(): Observable<ParticularService[]>{
        return this.http.get(this.url)
            .map(this.handleSuccess)
            .catch(this.handleError);
    }

    listCategories(): Observable<Category[]>{
        return this.http.get(this.urlCategories)
            .map(this.handleSuccess)
            .catch(this.handleError);
    }

    private handleSuccess(response: Response){
        let body = response.json();
        console.log("Response Type: " + response.type);
        console.log("Response: " + response);
        console.log("Sucesso: " + body);
        console.log("Data: " + JSON.stringify(body));
        return body || { };
    }

    private handleError (error: any) {
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }
}