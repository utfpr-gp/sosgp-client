"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var particular_service_model_1 = require("../model/particular-service.model");
var sos_particular_service_1 = require("./sos-particular.service");
var neighborhood_model_1 = require("../model/neighborhood.model");
var city_service_1 = require("../shared/services/city.service");
var SOSParticularFormComponent = (function () {
    function SOSParticularFormComponent(service, cityService) {
        this.service = service;
        this.cityService = cityService;
        this.errorMessage = "Houve um erro!";
        this.isUpdate = false;
        this.neighborhoods = [];
        this.cities = [];
        this.categories = [];
        /**
         *
         * Usado para habilitar o botão de submit
         *
         * @type {boolean}
         */
        this.submitted = false;
        /**
         *
         * Usado para resetar o formulário.
         * O valor é mudado para falso e depois novamente para verdadeiro.
         * Com isso, o formulário é recriado e os campos resetados após a criação de um modelo.
         *
         * @type {boolean}
         */
        this.active = true;
        this.entities = [];
    }
    /**
     *
     * Faz uma busca pelas cidades cadastradas.
     *
     * @returns {undefined}
     */
    SOSParticularFormComponent.prototype.ngOnInit = function () {
        this.listEntities();
        this.listCities();
        this.listCategories();
        this.resetForm();
    };
    SOSParticularFormComponent.prototype.ngAfterViewInit = function () {
    };
    SOSParticularFormComponent.prototype.listEntities = function () {
        var _this = this;
        this.service.listEntities().subscribe(function (entities) {
            _this.entities = entities;
        }, function (error) { return _this.errorMessage = error; });
    };
    SOSParticularFormComponent.prototype.listCities = function () {
        var _this = this;
        this.cityService.listCities().subscribe(function (entities) {
            _this.cities = entities;
            _this.neighborhoods = _this.cities[0].neighborhoods;
        }, function (error) { return _this.errorMessage = error; });
    };
    SOSParticularFormComponent.prototype.listCategories = function () {
        var _this = this;
        this.service.listCategories().subscribe(function (entities) {
            _this.categories = entities;
        }, function (error) { return _this.errorMessage = error; });
    };
    SOSParticularFormComponent.prototype.onNew = function () {
        this.resetForm();
    };
    SOSParticularFormComponent.prototype.onRemove = function (entity) {
        var index = this.entities.indexOf(entity);
        console.log("Removido indice " + index);
        this.isUpdate = false;
        this.delete(entity, index);
    };
    SOSParticularFormComponent.prototype.onEdit = function (entity) {
        this.model.id = entity.id;
        this.model.name = entity.name;
        this.model.phone = entity.phone;
        this.model.description = entity.description;
        this.model.category = entity.category;
        this.model.neighborhood.city = entity.neighborhood.city;
        this.model.neighborhood = entity.neighborhood;
        var index = this.entities.indexOf(entity);
        this.currentIndex = index;
        this.isUpdate = true;
    };
    SOSParticularFormComponent.prototype.resetForm = function () {
        var _this = this;
        this.active = false;
        this.model = new particular_service_model_1.ParticularService();
        setTimeout(function () { return _this.active = true; }, 0);
        this.isUpdate = false;
        this.model.neighborhood = new neighborhood_model_1.Neighborhood('');
        this.model.neighborhood.city = "Guarapuava";
        this.model.category = null;
    };
    SOSParticularFormComponent.prototype.onSubmit = function () {
        this.submitted = true;
        if (this.isUpdate) {
            this.update(this.model);
        }
        else {
            this.save(this.model);
        }
    };
    SOSParticularFormComponent.prototype.save = function (entity) {
        var _this = this;
        if (!entity) {
            return;
        }
        else {
            this.service.save(entity).subscribe(function (e) {
                _this.entities.push(e);
                _this.resetForm();
            }, function (error) { return _this.errorMessage = error; });
        }
    };
    SOSParticularFormComponent.prototype.update = function (entity) {
        var _this = this;
        if (!entity) {
            return;
        }
        else {
            this.service.update(entity).subscribe(function (e) {
                console.log("Corrente Indice: " + _this.currentIndex);
                //remove o antigo
                _this.entities.splice(_this.currentIndex, 1);
                //adiciona o novo
                _this.entities.push(e);
                _this.resetForm();
            }, function (error) { return _this.errorMessage = error; });
        }
    };
    SOSParticularFormComponent.prototype.delete = function (entity, index) {
        var _this = this;
        this.service.delete(entity.id).subscribe(function (res) {
            _this.entities.splice(index, 1);
        }, function (error) { return _this.errorMessage = error; });
    };
    /**
     *
     * Trata da seleção do bairro
     *
     * @param value
     */
    SOSParticularFormComponent.prototype.onChangeNeigh = function (value) {
        this.model.neighborhood.name = value;
    };
    SOSParticularFormComponent.prototype.onChangeCity = function (value) {
        this.model.neighborhood.city = value;
    };
    Object.defineProperty(SOSParticularFormComponent.prototype, "diagnostic", {
        get: function () {
            return JSON.stringify(this.model);
        },
        enumerable: true,
        configurable: true
    });
    SOSParticularFormComponent = __decorate([
        core_1.Component({
            selector: 'particular-service',
            templateUrl: '/app/admin/sos-particular-form.html',
            providers: [sos_particular_service_1.SOSParticularService, city_service_1.CityService]
        }), 
        __metadata('design:paramtypes', [sos_particular_service_1.SOSParticularService, city_service_1.CityService])
    ], SOSParticularFormComponent);
    return SOSParticularFormComponent;
}());
exports.SOSParticularFormComponent = SOSParticularFormComponent;
//# sourceMappingURL=sos-particular-form.component.js.map