import {Component, AfterViewInit, ElementRef, OnInit} from "@angular/core";
import {ParticularService} from "../model/particular-service.model";
import {SOSParticularService} from "./sos-particular.service";
import {City} from "../model/city.model";
import {Neighborhood} from "../model/neighborhood.model";
import {Category} from "../model/category.model";
import {CityService} from "../shared/services/city.service";

@Component({
    selector: 'particular-service',
    templateUrl: '/app/admin/sos-particular-form.html',
    providers:[SOSParticularService, CityService]
})
export class SOSParticularFormComponent implements OnInit, AfterViewInit{
    
    constructor(private service: SOSParticularService,
                private cityService: CityService){}

    /**
     * 
     * Faz uma busca pelas cidades cadastradas.
     * 
     * @returns {undefined}
     */
    ngOnInit():void {
        this.listEntities();
        this.listCities();
        this.listCategories();
        this.resetForm();
    }

    ngAfterViewInit():any {        
    }

    listEntities(){
        this.service.listEntities().subscribe(
            entities => {
                this.entities = entities;
            },
            error => this.errorMessage = <any> error
        )
    }

    listCities(){
        this.cityService.listCities().subscribe(
            entities => {
                this.cities = entities;
                this.neighborhoods = this.cities[0].neighborhoods;
            },
            error => this.errorMessage = <any> error
        )
    }

    listCategories(){
        this.service.listCategories().subscribe(
            entities => {
                this.categories = entities;
            },
            error => this.errorMessage = <any> error
        )
    }

    errorMessage:string = "Houve um erro!";
    model:ParticularService;

    currentIndex:number;
    isUpdate:boolean = false;
    neighborhoods:Neighborhood[] = [];
    cities:City[] = [];
    categories:Category[] = [];

    /**
     *
     * Usado para habilitar o botão de submit
     *
     * @type {boolean}
     */
    submitted = false;

    /**
     *
     * Usado para resetar o formulário.
     * O valor é mudado para falso e depois novamente para verdadeiro.
     * Com isso, o formulário é recriado e os campos resetados após a criação de um modelo.
     *
     * @type {boolean}
     */
    active = true;
    entities:ParticularService[] = [];
    
    onNew(){        
        this.resetForm();        
    }

    onRemove(entity){
        let index = this.entities.indexOf(entity);
        console.log("Removido indice " + index);
        this.isUpdate = false;
        this.delete(entity, index);
    }

    onEdit(entity){
        this.model.id = entity.id;
        this.model.name = entity.name;
        this.model.phone = entity.phone;
        this.model.description = entity.description;
        this.model.category = entity.category;
        this.model.neighborhood.city = entity.neighborhood.city;
        this.model.neighborhood = entity.neighborhood;

        let index = this.entities.indexOf(entity);
        this.currentIndex = index;
        this.isUpdate = true;
    }


    resetForm(){
        this.active = false;
        this.model = new ParticularService();
        setTimeout(() => this.active = true, 0);
        this.isUpdate = false;
        this.model.neighborhood = new Neighborhood('');
        this.model.neighborhood.city = "Guarapuava";
        this.model.category = null;
    }

    onSubmit(){
        this.submitted = true;
        if(this.isUpdate){
            this.update(this.model);
        }
        else{
            this.save(this.model);
        }
    }

    save(entity: ParticularService){
        if(!entity){
            return;
        }
        else{
            this.service.save(entity).subscribe(
                e => {
                    this.entities.push(e);
                    this.resetForm();
                },
                error => this.errorMessage = <any> error
            )
        }
    }

    update(entity: ParticularService){

        if(!entity){
            return;
        }
        else{
            this.service.update(entity).subscribe(
                e => {
                    console.log("Corrente Indice: " + this.currentIndex);

                    //remove o antigo
                    this.entities.splice(this.currentIndex, 1);

                    //adiciona o novo
                    this.entities.push(e);
                    this.resetForm();
                },
                error => this.errorMessage = <any> error
            )
        }
    }

    delete(entity: ParticularService, index: number){
        this.service.delete(entity.id).subscribe(
            res => {
                this.entities.splice(index, 1);
            },
            error => this.errorMessage = <any> error
        )
    }

    /**
     * 
     * Trata da seleção do bairro
     * 
     * @param value
     */
    onChangeNeigh(value){
        this.model.neighborhood.name = value;
    }

    onChangeCity(value){
        this.model.neighborhood.city = value;
    }

    get diagnostic(){
        return JSON.stringify(this.model);
    }
}