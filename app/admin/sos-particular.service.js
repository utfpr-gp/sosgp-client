"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var Rx_1 = require("rxjs/Rx");
var routes_server_util_1 = require("../util/routes-server.util");
var SOSParticularService = (function () {
    function SOSParticularService(http) {
        this.http = http;
        this.url = routes_server_util_1.RoutesServerUtil.URL_API + "particular-services";
        this.urlCategories = routes_server_util_1.RoutesServerUtil.URL_API + "categorias";
    }
    SOSParticularService.prototype.save = function (entity) {
        var body = JSON.stringify(entity);
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.post(this.url, body, options)
            .map(this.handleSuccess)
            .catch(this.handleError);
    };
    SOSParticularService.prototype.update = function (entity) {
        var body = JSON.stringify(entity);
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.put(this.url, body, options)
            .map(this.handleSuccess)
            .catch(this.handleError);
    };
    SOSParticularService.prototype.delete = function (id) {
        var url = this.url + "/" + id;
        return this.http.delete(url)
            .map(function (response) {
            console.log("Response Type: " + response.type);
            console.log("Response: " + response);
            return {};
        })
            .catch(this.handleError);
    };
    SOSParticularService.prototype.listEntities = function () {
        return this.http.get(this.url)
            .map(this.handleSuccess)
            .catch(this.handleError);
    };
    SOSParticularService.prototype.listCategories = function () {
        return this.http.get(this.urlCategories)
            .map(this.handleSuccess)
            .catch(this.handleError);
    };
    SOSParticularService.prototype.handleSuccess = function (response) {
        var body = response.json();
        console.log("Response Type: " + response.type);
        console.log("Response: " + response);
        console.log("Sucesso: " + body);
        console.log("Data: " + JSON.stringify(body));
        return body || {};
    };
    SOSParticularService.prototype.handleError = function (error) {
        var errMsg = (error.message) ? error.message :
            error.status ? error.status + " - " + error.statusText : 'Server error';
        console.error(errMsg); // log to console instead
        return Rx_1.Observable.throw(errMsg);
    };
    SOSParticularService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], SOSParticularService);
    return SOSParticularService;
}());
exports.SOSParticularService = SOSParticularService;
//# sourceMappingURL=sos-particular.service.js.map