"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var router_1 = require("@angular/router");
var sos_category_item_component_1 = require("../sos-category/sos-category-item.component");
var AdminHomeComponent = (function () {
    function AdminHomeComponent() {
    }
    AdminHomeComponent.prototype.ngOnInit = function () { };
    AdminHomeComponent = __decorate([
        core_1.Component({
            selector: 'admin',
            templateUrl: '/app/admin/admin-home.html',
            directives: [router_1.ROUTER_DIRECTIVES, sos_category_item_component_1.SOSCategoryItemComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], AdminHomeComponent);
    return AdminHomeComponent;
}());
exports.AdminHomeComponent = AdminHomeComponent;
//# sourceMappingURL=admin-home.component.js.map