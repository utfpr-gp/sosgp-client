///<reference path="../../node_modules/rxjs/add/operator/toPromise.d.ts"/>
import {Injectable} from "@angular/core";
import {Category} from "../model/category.model";
import {Http, Response, RequestOptions, Headers} from "@angular/http";
import '../util/rxjs-operators';
import {Observable} from "rxjs/Rx";

@Injectable()
export class CategoryService{

    private url = "http://localhost:8080/rest-archetype/api/categorias";

    constructor(private http: Http){
    }

    addCategory(category: Category): Observable<Category>{
        let body = JSON.stringify(category);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(this.url, body, options)
            .map(this.handleSuccess)
            .catch(this.handleError);
    }

    deleteCategory(id:number):Observable<{}>{
        let url:string = this.url + "/" + id;
        return this.http.delete(url)
            .map(response => {
                console.log("Response Type: " + response.type);
                console.log("Response: " + response);
                return {};
            })
            .catch(this.handleError);
    }
    
    getCategories(): Observable<Category[]>{
        return this.http.get(this.url)
            .map(this.handleSuccess)
            .catch(this.handleError);
    }

    private handleSuccess(response: Response){
        let body = response.json();
        console.log("Response Type: " + response.type);
        console.log("Response: " + response);
        console.log("Sucesso: " + body);
        console.log("Data: " + JSON.stringify(body));
        return body || { };
    }

    private handleError (error: any) {
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }
}
