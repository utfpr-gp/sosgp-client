import {Component, OnInit} from "@angular/core";
import {Category} from "../model/category.model";
import {CategoryService} from "./category.service";

@Component({
    selector: "register-category",
    templateUrl: "/app/admin/category-form.html",
    providers:[CategoryService]
})
export class CategoryFormComponent implements OnInit{
    constructor(private categoryService: CategoryService){
    }
    
    errorMessage:string = "Houve um erro!";
    model:Category = new Category();
    currentIndex:number;
    isUpdate:boolean = false;

    /**
     *
     * Usado para habilitar o botão de submit
     *
     * @type {boolean}
     */
    submitted = false;
    /**
     *
     * Usado para resetar o formulário.
     * O valor é mudado para falso e depois novamente para verdadeiro.
     * Com isso, o formulário é recriado e os campos resetados após a criação de um modelo.
     *
     * @type {boolean}
     */
    active = true;
    categories:Category[] = [];

    onSubmit(){
        this.submitted = true;
        if(this.isUpdate){
            this.updateCategory(this.model);
        }
        else{
            this.saveCategory(this.model);
        }

    }

    get diagnostic(){
        return JSON.stringify(this.model);
    }

    saveCategory(category: Category){
        if(!category){
            return;
        }
        else{
            this.categoryService.addCategory(category).subscribe(
                cat => {
                    this.categories.push(cat);
                    this.resetForm();
                },
                error => this.errorMessage = <any> error
            )
        }
    }

    updateCategory(category: Category){

        if(!category){
            return;
        }
        else{
            this.categoryService.addCategory(category).subscribe(
                cat => {
                    console.log("Corrente Indice: " + this.currentIndex);
                    console.log("Size antes " + this.categories.length);
                    //remove o antigo
                    this.categories.splice(this.currentIndex, 1);
                    console.log("Size depois " + this.categories.length);
                    //adiciona o novo
                    this.categories.push(cat);
                    this.resetForm();
                },
                error => this.errorMessage = <any> error
            )
        }
    }

    deleteCategory(category: Category, index: number){
        this.categoryService.deleteCategory(category.id).subscribe(
            res => {
                this.categories.splice(index, 1);
            },
            error => this.errorMessage = <any> error
        )
    }

    resetForm(){
        this.active = false;
        this.model.id = undefined;
        this.model.name = "";
        setTimeout(() => this.active = true, 0);
        this.isUpdate = false;
    }

    handleSuccess(cat: Category){
        this.categories.push(cat);
        this.model.name = "";
        console.log("Categorias: " + this.categories);
    }
    
    getCategories(){
        this.categoryService.getCategories().subscribe(
            categories => this.categories = categories,
            error => this.errorMessage = <any> error 
        )
    }

    newCategory(){
        this.model = new Category();
        this.isUpdate = false;
    }

    remove(category){
        let index = this.categories.indexOf(category);
        console.log("Removido indice " + index);
        this.isUpdate = false;
        this.deleteCategory(category, index);
    }

    edit(category){
        this.model.id = category.id;
        this.model.name = category.name;
        let index = this.categories.indexOf(category);
        this.currentIndex = index;
        this.isUpdate = true;
    }

    ngOnInit() {
        this.getCategories();
    }
}
