import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'sos-chat',
    templateUrl: '/app/chat.html'
})
export class ChatComponent implements OnInit {

    content: string;

    constructor() { }

    ngOnInit() { }

}
