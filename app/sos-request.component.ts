import { Component, OnInit } from '@angular/core';
import {TitleComponent} from "./shared/components/title.component";
import {ActivatedRoute, Router} from "@angular/router";
import {Anonymous} from "./model/user.model";
import {MapComponent} from "./map/map.component";
import {Location} from "./model/location.model";
import {City} from "./model/city.model";
import {CityService} from "./shared/services/city.service";
import {ChatComponent} from "./chat.component";
import {SOSMessage} from "./model/message.model";

@Component({
    selector: 'sos-request',
    templateUrl: '/app/sos-request.html',
    directives: [TitleComponent, MapComponent, ChatComponent],
    providers: [CityService]
})
export class SOSRequestComponent implements OnInit {
    private sub: any;

    title:string;
    icon:string;

    errorMessage:string = "Houve um erro!";
    model:Anonymous = new Anonymous();
    location:Location = new Location();
    occurrence: string;
    completeAddress:string;
    messages:SOSMessage[] = [];

    submitted = false;
    active = true;

    constructor(private route: ActivatedRoute,
                private router: Router) { }

    ngOnInit() {
        this.model.isMasculine = true;
        this.model.isVictim = true;
        this.sub = this.route.params.subscribe(
            params => {
                this.title = decodeURI(params['title']);//remove %20 quando a página é atualizada
                this.icon = params['icon'];
            }
        );

        this.messages.push(new SOSMessage("bla bla bla", "eu"));
        this.messages.push(new SOSMessage("bla bla bla", "samu"));
        this.messages.push(new SOSMessage("bla bla bla", "eu"));
        this.messages.push(new SOSMessage("bla bla bla", "samu"));
        this.messages.push(new SOSMessage("bla bla bla", "samu"));


    }

    /**
     *
     * Adiciona as mensagens na ordem de chegada(caso tenha vindo do servidor) ou
     * digitada.
     *
     * @param msg
     */
    addMessage(msg: SOSMessage){
        this.messages.push(msg);
    }

    addUserMessage(msg:string){
        this.messages.push(new SOSMessage(msg, "eu"));
    }

    /**
     *
     * Ao retornar com sucesso, esconder o formulário de identificação e de descrição.
     * Mostrar o de chat e embaixo o mapa.
     *
     */
    onSubmit(){
        this.submitted = true;
        //this.save(this.model);

        //sucesso
        this.addMessage(new SOSMessage(this.occurrence, "eu"));

    }

    onChange(value: boolean){
        this.model.isMasculine = value;
    }

    onChangeVictim(value: boolean){
        this.model.isVictim = value;
    }


    /**
     *
     * Invocado pelo componente mapa via @Output
     * Executado quando a posição corrente do usuário é descoberta ou alterada por
     * conta de locomoções.
     * Atualiza o campo de texto de endereço.
     * Retorna um JSON
     *
     * @param $event
     */
    onAddressChange(location){
        this.location = location;
    }


    
}
