import {Component, AfterViewInit, ElementRef} from '@angular/core';
import {ROUTER_DIRECTIVES} from "@angular/router";
import {RoutesClientUtil} from "./util/routes-client.util";
import {SOSRequestComponent} from "./sos-request.component";

declare var jQuery:any;

@Component({
    selector: 'my-app',
    directives: [ROUTER_DIRECTIVES],
    templateUrl: '/app/app.html',
    precompile: [SOSRequestComponent]
})
export class AppComponent implements AfterViewInit{
        
    constructor(private elRef: ElementRef){

    }

    ngAfterViewInit():any {
        jQuery(this.elRef.nativeElement).find('.button-collapse').sideNav();
        jQuery(this.elRef.nativeElement).find('select').material_select();
        //document.querySelector('select').material_select();

    }

}

