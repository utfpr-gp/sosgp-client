import {Component} from "@angular/core";
import {SOSCategoryItemComponent} from "./sos-category-item.component";
import {ROUTER_DIRECTIVES} from "@angular/router";

@Component({
    selector: "sos-services",
    templateUrl: "/app/sos-category/sos-category-list.html",
    directives:[SOSCategoryItemComponent, ROUTER_DIRECTIVES]
})
export class SOSCategoryListComponent{
        
}
