import {Component, Input, OnInit} from "@angular/core";
@Component({
    selector: 'sos-item',
    templateUrl: "/app/sos-category/sos-category-item.html",
})
export class SOSCategoryItemComponent implements OnInit{
    
    @Input() icon: string;
    @Input() title:string;

    @Input("background-color") backColor:string;
    @Input("text-color") textColor:string;

    constructor(){
    }

    ngOnInit():any {
    }
}
