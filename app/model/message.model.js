"use strict";
var SOSMessage = (function () {
    function SOSMessage(message, sender) {
        this.message = message;
        this.sender = sender;
    }
    return SOSMessage;
}());
exports.SOSMessage = SOSMessage;
//# sourceMappingURL=message.model.js.map