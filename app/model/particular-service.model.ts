import {City} from "./city.model";
import {Neighborhood} from "./neighborhood.model";
import {Category} from "./category.model";
export class ParticularService{

    constructor(public name?: string,
                public phone?:string,
                public description?:string,
                public id?:number,
                public neighborhood?:Neighborhood,
                public category?:Category){}

    get categoryProperty() {
        return this.category;
    }

    set categoryProperty(value) {
        this.category = value;
    }
}

