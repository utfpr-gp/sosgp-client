"use strict";
var Neighborhood = (function () {
    function Neighborhood(name, city) {
        this.name = name;
        this.city = city;
    }
    return Neighborhood;
}());
exports.Neighborhood = Neighborhood;
//# sourceMappingURL=neighborhood.model.js.map