"use strict";
var Anonymous = (function () {
    function Anonymous(name, isMasculine, age, isVictim) {
        this.name = name;
        this.isMasculine = isMasculine;
        this.age = age;
        this.isVictim = isVictim;
    }
    return Anonymous;
}());
exports.Anonymous = Anonymous;
//# sourceMappingURL=user.model.js.map