"use strict";
var ParticularService = (function () {
    function ParticularService(name, phone, description, id, neighborhood, category) {
        this.name = name;
        this.phone = phone;
        this.description = description;
        this.id = id;
        this.neighborhood = neighborhood;
        this.category = category;
    }
    Object.defineProperty(ParticularService.prototype, "categoryProperty", {
        get: function () {
            return this.category;
        },
        set: function (value) {
            this.category = value;
        },
        enumerable: true,
        configurable: true
    });
    return ParticularService;
}());
exports.ParticularService = ParticularService;
//# sourceMappingURL=particular-service.model.js.map