
export class Location{

    constructor(public street?:string, public number?:number, public neigh?: string, public city?: string, public state?: string, public lat?: number, public lon?: number){
        this.number == undefined ? 0 : number;
        this.city == undefined ? "Guarapuava" : city;
        this.state == undefined ? "Paraná" : state;
    }
}
