"use strict";
var Location = (function () {
    function Location(street, number, neigh, city, state, lat, lon) {
        this.street = street;
        this.number = number;
        this.neigh = neigh;
        this.city = city;
        this.state = state;
        this.lat = lat;
        this.lon = lon;
        this.number == undefined ? 0 : number;
        this.city == undefined ? "Guarapuava" : city;
        this.state == undefined ? "Paraná" : state;
    }
    return Location;
}());
exports.Location = Location;
//# sourceMappingURL=location.model.js.map