import {Neighborhood} from "./neighborhood.model";
export class City{

    constructor(public name?:string, public neighborhoods?:Neighborhood[]){

    }
}
