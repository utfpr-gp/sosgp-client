"use strict";
var City = (function () {
    function City(name, neighborhoods) {
        this.name = name;
        this.neighborhoods = neighborhoods;
    }
    return City;
}());
exports.City = City;
//# sourceMappingURL=city.model.js.map