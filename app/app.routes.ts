import { provideRouter, RouterConfig } from '@angular/router';
import {EmergencyNumbersComponent} from "./emergency-number/emergency-numbers-list.component";
import {HowWorksComponent} from "./about/how-works.component";
import {SOSCategoryListComponent} from "./sos-category/sos-category-list.component";
import {SOSParticularCategoryListComponent} from "./sos-particular/sos-particular-category-list.component";
import {CategoryFormComponent} from "./admin/category-form.component";
import {SOSParticularFormComponent} from "./admin/sos-particular-form.component";
import {SOSParticularItemListComponent} from "./sos-particular/sos-particular-item-list.component";
import {AdminHomeComponent} from "./admin/admin-home.component";
import {SOSRequestComponent} from "./sos-request.component";

export const routes: RouterConfig = [
    {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
    },
    { path: 'home', component:  SOSCategoryListComponent},
    { path: 'numeros-emergencia', component: EmergencyNumbersComponent },
    { path: 'how-works', component: HowWorksComponent },
    { path: 'lista-categorias-servicos-particulares', component: SOSParticularCategoryListComponent },
    { path: 'cadastrar-categorias', component: CategoryFormComponent },
    { path: 'cadastrar-servicos-particulares', component: SOSParticularFormComponent },
    { path: 'lista-itens-servicos-particulares/:category/:icon/:back', component: SOSParticularItemListComponent },
    { path: 'admin', component: AdminHomeComponent },
    { path: 'chamado/:title/:icon/:background-color/:text-color', component: SOSRequestComponent }
    
];

export const APP_ROUTER_PROVIDERS = [
    provideRouter(routes)
];
