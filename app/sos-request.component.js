"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var title_component_1 = require("./shared/components/title.component");
var router_1 = require("@angular/router");
var user_model_1 = require("./model/user.model");
var map_component_1 = require("./map/map.component");
var location_model_1 = require("./model/location.model");
var city_service_1 = require("./shared/services/city.service");
var chat_component_1 = require("./chat.component");
var message_model_1 = require("./model/message.model");
var SOSRequestComponent = (function () {
    function SOSRequestComponent(route, router) {
        this.route = route;
        this.router = router;
        this.errorMessage = "Houve um erro!";
        this.model = new user_model_1.Anonymous();
        this.location = new location_model_1.Location();
        this.messages = [];
        this.submitted = false;
        this.active = true;
    }
    SOSRequestComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.model.isMasculine = true;
        this.model.isVictim = true;
        this.sub = this.route.params.subscribe(function (params) {
            _this.title = decodeURI(params['title']); //remove %20 quando a página é atualizada
            _this.icon = params['icon'];
        });
        this.messages.push(new message_model_1.SOSMessage("bla bla bla", "eu"));
        this.messages.push(new message_model_1.SOSMessage("bla bla bla", "samu"));
        this.messages.push(new message_model_1.SOSMessage("bla bla bla", "eu"));
        this.messages.push(new message_model_1.SOSMessage("bla bla bla", "samu"));
        this.messages.push(new message_model_1.SOSMessage("bla bla bla", "samu"));
    };
    /**
     *
     * Adiciona as mensagens na ordem de chegada(caso tenha vindo do servidor) ou
     * digitada.
     *
     * @param msg
     */
    SOSRequestComponent.prototype.addMessage = function (msg) {
        this.messages.push(msg);
    };
    SOSRequestComponent.prototype.addUserMessage = function (msg) {
        this.messages.push(new message_model_1.SOSMessage(msg, "eu"));
    };
    /**
     *
     * Ao retornar com sucesso, esconder o formulário de identificação e de descrição.
     * Mostrar o de chat e embaixo o mapa.
     *
     */
    SOSRequestComponent.prototype.onSubmit = function () {
        this.submitted = true;
        //this.save(this.model);
        //sucesso
        this.addMessage(new message_model_1.SOSMessage(this.occurrence, "eu"));
    };
    SOSRequestComponent.prototype.onChange = function (value) {
        this.model.isMasculine = value;
    };
    SOSRequestComponent.prototype.onChangeVictim = function (value) {
        this.model.isVictim = value;
    };
    /**
     *
     * Invocado pelo componente mapa via @Output
     * Executado quando a posição corrente do usuário é descoberta ou alterada por
     * conta de locomoções.
     * Atualiza o campo de texto de endereço.
     * Retorna um JSON
     *
     * @param $event
     */
    SOSRequestComponent.prototype.onAddressChange = function (location) {
        this.location = location;
    };
    SOSRequestComponent = __decorate([
        core_1.Component({
            selector: 'sos-request',
            templateUrl: '/app/sos-request.html',
            directives: [title_component_1.TitleComponent, map_component_1.MapComponent, chat_component_1.ChatComponent],
            providers: [city_service_1.CityService]
        }), 
        __metadata('design:paramtypes', [router_1.ActivatedRoute, router_1.Router])
    ], SOSRequestComponent);
    return SOSRequestComponent;
}());
exports.SOSRequestComponent = SOSRequestComponent;
//# sourceMappingURL=sos-request.component.js.map