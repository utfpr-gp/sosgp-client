import {Component, OnInit, Input} from '@angular/core';

@Component({
    selector: 'sos-title',
    templateUrl: '/app/shared/components/title.html'
})
export class TitleComponent implements OnInit {
    @Input() icon: string;
    @Input() title:string;

    @Input("background-color") backColor:string;
    @Input("text-color") textColor:string;

    constructor() { }

    ngOnInit() { }
}
