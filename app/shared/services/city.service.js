"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var Rx_1 = require("rxjs/Rx");
var http_1 = require("@angular/http");
var routes_server_util_1 = require("../../util/routes-server.util");
var CityService = (function () {
    function CityService(http) {
        this.http = http;
        this.urlCities = routes_server_util_1.RoutesServerUtil.URL_API + "cities";
    }
    CityService.prototype.listCities = function () {
        return this.http.get(this.urlCities)
            .map(this.handleSuccess)
            .catch(this.handleError);
    };
    CityService.prototype.handleSuccess = function (response) {
        var body = response.json();
        console.log("Response Type: " + response.type);
        console.log("Response: " + response);
        console.log("Sucesso: " + body);
        console.log("Data: " + JSON.stringify(body));
        return body || {};
    };
    CityService.prototype.handleError = function (error) {
        var errMsg = (error.message) ? error.message :
            error.status ? error.status + " - " + error.statusText : 'Server error';
        console.error(errMsg); // log to console instead
        return Rx_1.Observable.throw(errMsg);
    };
    CityService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], CityService);
    return CityService;
}());
exports.CityService = CityService;
//# sourceMappingURL=city.service.js.map