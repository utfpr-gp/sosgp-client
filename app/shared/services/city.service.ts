import { Injectable } from '@angular/core';
import {City} from "../../model/city.model";
import {Observable} from "rxjs/Rx";
import {Http, Response} from "@angular/http";
import {RoutesServerUtil} from "../../util/routes-server.util";

@Injectable()
export class CityService {

    private urlCities = RoutesServerUtil.URL_API + "cities";

    constructor(private http: Http) { }

    listCities(): Observable<City[]>{
        return this.http.get(this.urlCities)
            .map(this.handleSuccess)
            .catch(this.handleError);
    }

    private handleSuccess(response: Response){
        let body = response.json();
        console.log("Response Type: " + response.type);
        console.log("Response: " + response);
        console.log("Sucesso: " + body);
        console.log("Data: " + JSON.stringify(body));
        return body || { };
    }

    private handleError (error: any) {
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }

}
