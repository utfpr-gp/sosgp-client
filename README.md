# SOS-GP Client #

Este projeto está sendo desenvolvido com o _framework_ Angular e corresponde ao lado cliente do sistema SOS-GP.

O sistema SOS-GP tem como objetivo permitir que deficientes auditivos façam chamadas de emergência e urgência ao Corpo de Bombeiros da região de Guarapuava por meio de mensagens de texto, envio de imagens e o envio da sua localização através de GPS.

### Principais Funcionalidades ###

* Cadastro de usuários com comprovada necessidade especial.
* Cadastro do histórico de saúde dos usuários.
* Identificação geográfica da vítima.
* Acompanhamento geográfico da ambulância pela vítima ou responsáveis.
* Recursos de comunicação da vítima com atendentes do Corpo de Bombeiros.

### Tecnologias ###

* Angular
* Phonegap/Cordova

### Ferramentas ###

* WebStorm
* NPM

### Manual de Execução ###

+ Clonar o repositório com `git clone`
+ Abrir o projeto no Editor de Código WebStorm
+ Instalar as dependências definidas no arquivo `package.json` via `npm`
    - Entrar no diretório raiz do projeto via prompt de comando: `cd sosgp-client`
    - Digitar o comando `npm install`
    - Este comando vai criar o diretório `node_modules` com as dependências do Angular, Materialize e outros.
+ Para executar o projeto no servidor local `lite-server`, digite `npm start`
    - O sistema será apresentado no endereço `localhost:3000`  

### Versão Corrente ###
0.0.1 - 21/07/2016